"use strict";

const Bluebird = require("bluebird"); 
const UserModel = require("../models/user_model");
const constants = require("../utils/constants");
const utils = require("../utils/utils");
const crypto = require('crypto');
const mongodb = require('mongodb');

/*
 *  Create New User
 */
const createNewUser = function (userData) {
    return Bluebird.try(async() => { 
        // encrypting the password Before Saving in SSH
        userData.password = utils.encryptStringCrypt(userData.password);
        let User = new UserModel(userData);
        return User.save().then((isSaved)=>{    
            return UserModel.findById(isSaved._id).lean();
        });
    }).catch((error) => {   
        console.error(error);
        return error;
    });
};

/*
 *  Check Email Exist
 */
const checkEmailExists = function(email){
    return Bluebird.try(async() => {
        let userInfo = await UserModel.findOne({email:email}).lean();
        return userInfo;
    }).catch((error) => {   
        console.error(error);
        return error;
    });
};

/*
 *  Check Mobile Number Exist
 */
const checkNumberExists = function(mobile_number){
    return Bluebird.try(async() => {
        let userInfo = await UserModel.findOne({mobile_number:mobile_number}).lean();
        return userInfo;
    }).catch((error) => {   
        console.error(error);
        return error;
    });
};

/*
 *  Get User By _id
 */
const findUserById = function(user_id){
    return Bluebird.try(async() => {
      let userInfo = await UserModel.findById(user_id).lean();
      return userInfo;
    }).catch((error) => {   
        console.error(error);
        return error;
    });
};

/*
 *  Update User Data
 */
const updatedUserData = function(userId,data){
    return Bluebird.try(async() => {
        return UserModel.findOneAndUpdate({_id:userId},{$set:data},{new:true}).lean();
    }).catch((error) => {
        console.error(error);
        return error;
    });
};

/*
 *  Check Email Exist With User _id
 */
const checkEmailExistsForUpdateUser = function(user_id,email){
    return Bluebird.try(async() => {
        let userInfo = await UserModel.findOne({$and: [{email:email},{'_id': {$ne : new mongodb.ObjectId(user_id)}}]}).lean();
        return userInfo;
    }).catch((error) => {
        console.error(error);
        return error;
    });
};


module.exports = {
    createNewUser:createNewUser,
    checkEmailExists:checkEmailExists,
    checkNumberExists:checkNumberExists,
    findUserById:findUserById,
    updatedUserData:updatedUserData,
    checkEmailExistsForUpdateUser:checkEmailExistsForUpdateUser
};