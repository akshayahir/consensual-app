"use strict";

const Bluebird = require("bluebird"); 
const constants = require("../utils/constants");
const accountSid = constants.DEFAULT_TWILIO_SETTINGS.accountSid;
const authToken = constants.DEFAULT_TWILIO_SETTINGS.authToken;
const client = require('twilio')(accountSid, authToken);
const fs = require("fs");

const sendVerificationCode = function(postData){
    return Bluebird.try(async() =>{
        console.log('test');
        client.messages
        .create({
            body: 'Consensual App verification code : '+postData.otp+".",
            from: constants.DEFAULT_TWILIO_SETTINGS.twillio_number,
            to: "+91"+""+postData.mobile_number
        })
        .then(async(message)=>{ 
            console.log("message",message);   
            return message;
        });
    }).catch((error) => {
        console.error(error);
        return error;
    });
};

const unlinkImage = function(imagePath){
    return Bluebird.try(async() => {
        fs.unlink(imagePath, (err) => {
            if (err) {
                console.error(err);
                return false;
            }else{
                console.log("unlinked successfull");
                return true;
            }
        })
    }).catch((error) => {
        console.error(error);
        return error;
    });
};


module.exports = {
    sendVerificationCode:sendVerificationCode,
    unlinkImage:unlinkImage
};