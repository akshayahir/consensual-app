"use strict";

const router = require("express-promise-router")();
const Bluebird = require("bluebird");
const UserController = require("../controllers/userController");
const utils = require("../utils/utils");
const constants = require("../utils/constants");
const customErrors = require("../utils/errors");
const otpGenerator = require('otp-generator');
const commonFunctionController = require("../controllers/commonFunctions");
const authetication = require("../middleware/authentication");
const multerSettings = require("../utils/multer-settings");
const path = require("path");

let uploadUserImageConfig = multerSettings.uploadUserImageConfig;

/*
 *  User Registration
 */
router.post("/register", function (req, res, next) {
    return Bluebird.try(async() => {
        let upload = Bluebird.promisify(uploadUserImageConfig);
        return upload(req, res).then(async(data) => {
            let response = {success:false}; 
            let postData = req.body;
            postData.email = postData.email.toLowerCase();
            let checkEmailExists = await UserController.checkEmailExists(postData.email);
            if(!checkEmailExists){
                let checkNumberExists = await UserController.checkNumberExists(postData.mobile_number);
                if(!checkNumberExists){
                    if(req.files && (req.files.profile_image)){
                        postData.profile_image = req.files.profile_image[0].filename;
                    }
                    postData.otp = Math.floor(1000 + Math.random() * 9000);
                    let sendVerificationCode = await commonFunctionController.sendVerificationCode(postData);
                    let isUserAdded = await UserController.createNewUser(postData);
                    if(isUserAdded){
                        response.success = true;
                        response.msg = 'OTP has been send to your mobile number, Please enter otp to verify your account';
                        response.data = isUserAdded
                    }else{
                        response.success = false;
                        response.msg = constants.COMMON_ERROR_MESSAGES.DEFAULT_ERROR;
                    }
                }else{
                    response.success = false;
                    response.msg = customErrors.ERROR_CODES.DATABASE_ERROR.MOBILE_NUMBER_EXISTS.MESSAGE;
                }
            }else{
                response.success = false;
                response.msg = customErrors.ERROR_CODES.DATABASE_ERROR.EMAIL_EXISTS.MESSAGE;
            }
            return res.status(200).send(response);
        });
    });
});

/*
 *  User Login
 */
router.post("/login", function (req, res, next) {
    return Bluebird.try(async() => {
        let postData = req.body;
        let response = {success:false};
        let userFound = await UserController.checkNumberExists(postData.mobile_number);
        if(userFound){
            let isPasswordValid = utils.matchPassword(postData.password,userFound.password);
            if(isPasswordValid){
                if (userFound.status == "active") {
                    if (userFound.profile_image && userFound.profile_image != '') {
                        userFound.profile_image = process.env.BASE_URL+"public/user-images/"+userFound.profile_image;
                    }
                    let UserDetailsToken = await utils.signToken(userFound);
                    response.success = true;
                    response.data = userFound;
                    response.token = UserDetailsToken;
                }else{
                    if (userFound.status == "deactivated") {
                        response.success = false;
                        response.msg = "This account has been deactivated";
                        response.data = userFound;
                    }else{
                        response.success = false;
                        response.msg = "Please verify your mobile number";
                        response.data = userFound;
                    }
                }
            }else{
                response.success = false;
                response.msg = customErrors.ERROR_CODES.DATABASE_ERROR.INVALID_PASSWORD.MESSAGE;
            }
        }else{
            response.success = false;
            response.msg = customErrors.ERROR_CODES.DATABASE_ERROR.INVALID_EMAIL_OR_USERNAME.MESSAGE;
        }
        return res.status(200).send(response);
    });
});

/*
 *  Verify OTP For Mobile Number Verification
 */
router.post("/verify-otp", function (req, res, next) {
    return Bluebird.try(async() => {
        let response = {success:false}; 
        let postData = req.body;
        let userFound = await UserController.findUserById(postData._id);
        if(userFound){
            if(postData.otp && postData.otp != '' && userFound.otp == postData.otp){
                let updateData = {
                    status:"active"
                }
                let UpdateUserDetails = await UserController.updatedUserData(userFound._id,updateData);
                if(UpdateUserDetails){
                    let UserDetailsToken = await utils.signToken(UpdateUserDetails);
                    response.success = true;
                    response.msg = "Verified successfully";
                    response.data = UpdateUserDetails;
                    response.token = UserDetailsToken;
                }
            }else{
                response.success = false;
                response.msg = "Invalid OTP code, Please try again";
            }
        }else{
            response.success = false;
            response.msg = constants.COMMON_ERROR_MESSAGES.DEFAULT_ERROR;
        }
        return res.status(200).send(response);
    });
});

/*
 *  Resend OTP For Mobile Number Verification
 */
router.post("/resend-otp", function (req, res, next) {
    return Bluebird.try(async() => {
        let postData = req.body;
        let response = {success:false};
        let userFound = await UserController.findUserById(postData._id);
        if(userFound){
            let otp = Math.floor(1000 + Math.random() * 9000);
            let updateData = {
                otp:otp
            }
            let UpdateUserDetails = await UserController.updatedUserData(userFound._id,updateData);
            if(UpdateUserDetails){

                let sendVerificationCode = await commonFunctionController.sendVerificationCode(UpdateUserDetails);
                response.success = true;
                response.msg = "Verification code send successfull";
                response.data = UpdateUserDetails;
            }else{
                response.success = false;
                response.msg = constants.COMMON_ERROR_MESSAGES.DEFAULT_ERROR;
            }
        }else{
            response.success = false;
            response.msg = constants.COMMON_ERROR_MESSAGES.DEFAULT_ERROR;
        }
        return res.status(200).send(response); 
    });
});

/*
 *  Change Password
 */
router.post("/change-password", [authetication.authenticate], function (req, res, next) {
    return Bluebird.try(async() => {
        let postData = req.body;
        let response = {success:false};
        let isPasswordValid = utils.matchPassword(postData.old_password,req.user.password);
        if(isPasswordValid){
            if(postData.new_password && postData.confirm_password){
                if(postData.new_password == postData.confirm_password){
                    postData.new_password = await utils.encryptStringCrypt(postData.new_password);
                    let updateData = {
                        password: postData.new_password
                    };
                    let updateUserDetails = await UserController.updatedUserData(req.user._id,updateData);
                    if(updateUserDetails){
                        response.success = true;
                        response.msg = 'Password changed successfully';
                        response.data = updateUserDetails;
                    }else{
                        response.success = false;
                        response.msg = constants.COMMON_ERROR_MESSAGES.DEFAULT_ERROR;
                    }
                }else{
                    response.success = false;
                    response.msg = 'Confirm password did not match, Please try again';
                }
            }else{
                response.success = false;
                response.msg = 'All fields required';
            }
        }else{
            response.success = false;
            response.msg = "Please enter valid old password";
        }
        return res.status(200).send(response); 
    });
});

/*
 *  Update User Profile Data
 */
router.post("/update-user-details", [authetication.authenticate],function (req, res, next) {
    return Bluebird.try(async() => {
        let upload = Bluebird.promisify(uploadUserImageConfig);
        return upload(req, res).then(async(uploadData) => {
            let response = {};
            let postData = req.body;
            let user_id = req.user._id;
            if(req.files && (req.files.profile_image)){
                postData.profile_image = req.files.profile_image[0].filename;
            }
            if(postData.email && postData.email != ''){
                postData.email = postData.email.toLowerCase();
                let checkEmailExists = await UserController.checkEmailExistsForUpdateUser(user_id,postData.email);
                if(checkEmailExists){
                    response.success = false;
                    response.msg = customErrors.ERROR_CODES.DATABASE_ERROR.EMAIL_EXISTS.MESSAGE;
                    return res.status(200).send(response);
                }
            }
            let updatedDetails = await UserController.updatedUserData(req.user._id,postData);
            if (updatedDetails) {
                if (req.files && (req.files.profile_image) && req.user.profile_image && req.user.profile_image != '') {
                    let userUploadDirPath = path.join(__dirname, "..", constants.UPLOAD_DIR_PATH.USER_IMAGE);
                    let imagePath =  userUploadDirPath+"/"+req.user.profile_image;
                    commonFunctionController.unlinkImage(imagePath);
                }
                if (updatedDetails.profile_image && updatedDetails.profile_image != '') {
                    updatedDetails.profile_image = process.env.BASE_URL+"public/user-images/"+updatedDetails.profile_image;
                }
                response.success = true;
                response.data = updatedDetails;
                response.msg = constants.COMMON_MESSAGES.USER_DETAILS_UPDATED;
            }else{
                response.success = false;
                response.msg = customErrors.ERROR_CODES.UNKNOWN_ERROR.MESSAGE;
            }
            return res.status(200).send(response);
        }); 
    });
});

/*
 *  Forgot Password
 */
router.post("/forgot-password", function (req, res, next) {
    return Bluebird.try(async() => {
        let response = {success:false};
        let postData = req.body;
        if (postData.mobile_number && postData.mobile_number !='') {
            let checkUserExist = await UserController.checkNumberExists(postData.mobile_number);
            if(checkUserExist) {
                let otp = Math.floor(1000 + Math.random() * 9000);
                let updateData = {
                    otp:otp
                }
                let UpdateUserDetails = await UserController.updatedUserData(checkUserExist._id,updateData);
                if(UpdateUserDetails){
                    let sendVerificationCode = await commonFunctionController.sendVerificationCode(UpdateUserDetails);
                    response.success = true;
                    response.msg = "Verification code send successfull";
                    response.data = UpdateUserDetails;
                }else{
                    response.success = false;
                    response.msg = constants.COMMON_ERROR_MESSAGES.DEFAULT_ERROR;
                }
            }else{
                response.success = false;
                response.msg = 'Mobile number does not exist';
            }
        }else{
            response.success = false;
            response.msg = 'Mobile number cannot be empty';
        }
      return res.status(200).send(response);
    }); 
});

/*
 *  Verify Forgot password OTP
 */
router.post("/verify-forgot-password-otp", function (req, res, next) {
    return Bluebird.try(async() => {
        let response = {success:false}; 
        let postData = req.body;
        let userFound = await UserController.findUserById(postData._id);
        if(userFound){
            if(postData.otp && postData.otp != '' && userFound.otp == postData.otp){
                response.success = true;
                response.msg = "Verified successfully";
                response.data = userFound;
            }else{
                response.success = false;
                response.msg = "Invalid OTP code, Please try again";
            }
        }else{
            response.success = false;
            response.msg = constants.COMMON_ERROR_MESSAGES.DEFAULT_ERROR;
        }
        return res.status(200).send(response);
    });
});

/*
 *  Reset Password
 */
router.post("/reset-password", function (req, res, next) {
    return Bluebird.try(async() => {
        let response = {success:false};
        let postData = req.body;
        let userFound = await UserController.findUserById(postData._id);
        if(userFound){
            if(postData.password && postData.password != ''){
                postData.password = await utils.encryptStringCrypt(postData.password);
                let updateData = {
                    password: postData.password
                };
                let updateUserDetails = await UserController.updatedUserData(userFound._id,updateData);
                if(updateUserDetails){

                    if (updateUserDetails.profile_image && updateUserDetails.profile_image != '') {
                        updateUserDetails.profile_image = process.env.BASE_URL+"public/user-images/"+updateUserDetails.profile_image;
                    }

                    let UserDetailsToken = await utils.signToken(updateUserDetails);
                    response.success = true;
                    response.msg = constants.COMMON_MESSAGES.USER_PASSWORD_UPDATED;
                    response.data = updateUserDetails;
                    response.token = UserDetailsToken;
                }else{
                    response.success = false;
                    response.msg = constants.COMMON_ERROR_MESSAGES.DEFAULT_ERROR;
                }
            }else{
                response.success = false;
                response.msg = 'Password cannot be empty';
            }
        }else{
            response.success = false;
            response.msg = constants.COMMON_ERROR_MESSAGES.DEFAULT_ERROR;
        }
        return res.status(200).send(response);
    }); 
});

/*
 *  Get User Details
 */
router.get("/user-details", [authetication.authenticate],function (req, res, next) {
    return Bluebird.try(async() => {
        let response = {success:false};
        let postData = req.body;
        if (req.user.profile_image && req.user.profile_image != '') {
            req.user.profile_image = process.env.BASE_URL+"public/user-images/"+req.user.profile_image;
        }
        response.success = true;
        response.msg = "Success";
        response.data = req.user;
        return res.status(200).send(response);
    }); 
});

module.exports = router;