"use strict";

const mongoose = require("mongoose");
mongoose.Promise = require("bluebird");

const Schema = mongoose.Schema;

// @NOTE: geoJSon insert value like this { type: 'Point', coordinates: [-179.0, 0.0] }

const UsersSchema = new Schema({

    email:{
        type:String,
        unique: true,
        required:true
    },
    first_name: {
        type: String,
        required: true
    },
    last_name: {
        type: String,
        required: true
    },
    mobile_number: {
        type: Number,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    profile_image: {
        type: String,
        default:"",
        required:false
    },
    role: {
        type: String,
        enum: ["admin","user"],
        default:"user",
        required:false,
    },
    status: {
        type: String,
        enum: ["active","deactivated","pending"],
        default:"pending",
        required:false,
    },
    date_of_birth: {
        type: Date,
        required: true
    },
    address             : { 
        street_address  : String,
        city            : String,
        zip_code        : String
    },
    otp: {
        type: String,
        default:"",
        required: false
    },

}, { strict: true, timestamps: { createdAt: "createdAt", updatedAt: "updatedAt" } });

UsersSchema.on("index", (err) => { console.log(">>>>>>>>>>>>>>>", err); });
module.exports = mongoose.model("Users", UsersSchema);

